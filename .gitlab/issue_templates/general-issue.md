## Overview (be Specific)

> _Add in an overview of what you want to complete in this project or task. Be specific with what needs to be completed, fixed, or improved_

...

## Objectives (be Measurable and Achievable)

> _Use the "Child Items" functionality below to add in the specific tasks that you plan to complete. These should be clear milestones with a measurable outcome. Make sure you add an assignee (DRI) and a due date for each task. Add a start date to the task only once you've actually begun work on it._

Specific objectives, DRIs, start/due dates and more can be found in the child items below.

## Outcomes (be Relevant)

> _What are the outcomes that will be delivered when this project or task is completed? How does this align with broader values and goals?_
>
> _Opportunistic Attacks: This can include attacker objectives (gain access to x), proving a hypothesis (a specific service or behavior is exploitable), or other goals to reduce risk at GitLab. Try to aim for outcomes that have a broad impact. For example, finding one vulnerable public resource is good. Finding out how it got there, and providing a recommendation to prevent it in the future is better._
>
> _Research: This should include what the output of your research will be - such as a blog, conference talk, vulnerability disclosure, tool, etc._

...

## Target Completion Date (set a Time-bound target)

> _When will the project or task be completed? Use the native GitLab functionality to set a due date, and adjust as necessary. You can also use the text below to add any context to that date._

The completion target is set under "Due Date" on the right of the issue.

/confidential
