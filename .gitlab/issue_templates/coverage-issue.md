<!-- A quick description of the dates you're out, e.g. "I'll be OOO from X March until X April" (or the equivalent ISO8601 format dates) -->

## Intent

This issue describes how team members help each other take meaningful \[time away from work\]\[pto\]. The goal is for those taking PTO to not feel like they need to "catch up" before and after \[taking a well-deserved vacation\]\[time-off\].

It also helps for team members to note down what they were working on before 5+ days PTO, so they can seamlessly continue their work and check in on it when they return.

## Responsibilities

<!-- the things that you would normally be doing that someone else needs to be aware of/cover for you (with issue/MR/epic link) -->

<!-- NOTE Since this will be an internal issue visible outside of the Red Team, consider giving codenames to your stealthy initiatives if they are included here, or add them to a Slack thread -->

## :muscle: Coverage Tasks

Include specific issue links for tasks to be completed by your backups.

* Item 1 (with issue/MR/epic link)
* ... etc

## :blue_car: Parked tasks

List the larger responsibilities that aren't moving forward until you return.

* Item 1 (with issue/MR/epic link)
* ... etc

## :white_check_mark: Issue Tasks

### :o: Opening Tasks

- [ ] Assign to yourself
- [ ] Title the issue `Coverage for YOUR-NAME from yyyy-mm-dd until yyyy-mm-dd`
- [ ] Add any relevant references including direction pages, handbook pages, and so on
- [ ] Fill in the relevant sections with anything you'll want to track when you get back
- [ ] Share this issue in your section, stage, and group Slack channels, and with your manager
- [ ] (Optional) Link to this issue in the "Role Description" section of the Time Off by Deel coverage for your out of office dates
- [ ] Update your GitLab.com status with your out of office dates
- [ ] (Optional) Add an issue comment for your retrospective titled: `:recycle: Retrospective Thread`

### :x: Closing Tasks

- [ ] Assign back to yourself and remove others
- [ ] Review any items with your teammates, collaborators/counterparts and manager

<!--Do not remove the items below-->

\\\[time-off\\\]: https://about.gitlab.com/handbook/engineering/ux/how-we-work/#time-off \\\[pto\\\]: https://about.gitlab.com/handbook/paid-time-off/

cc @cmoberly