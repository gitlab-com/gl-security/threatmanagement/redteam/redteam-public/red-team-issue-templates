> _This template is used for [Purple Team Flash Operations](https://handbook.gitlab.com/handbook/security/security-operations/red-team/purple-teaming/#flash-operations)_

## Stage Summary

This template documents the Red Team emulating the specific threats identified in the Threat Identification stage.

## To-Do

- [ ] Schedule synchronous meetings when appropriate, filling in the "Synchronous Meetings" section below.
- [ ] Fill in the "Assets in Scope" section below.
- [ ] Execute the TTPs as agreed upon in the previous stages.
- [ ] Complete the Red Team operator logs. This can be done in the table below, or as individual comments in this issue.
- [ ] Ensure all log files are available for review.
- [ ] Conduct cleanup activities as required.

## Synchronous Meetings

> _This section should be used to plan meeting times and participants_

| Date/Time | Participants | Completed? |
| ----- | ----- | ----- |
| x | x | x |
| x | x | x |
| x | x | x |
| x | x | x |

## Assets in Scope

Let's identify the target assets that are in scope for this Purple Team Flash Operation.
Is it a host or an application?  Is it a software component like a library?

Some examples:

- Host: db.gitlab.com
- Library: Nokogiri
- Application: About.gitlab.com

## Red Team Operator Logs

> _This section should be filled out during the operation. Alternatively, the issue comments can be used as a running log of attack techniques._

| Date/Time | Operator Name | Source | Target | Description | IoC | Evidence |
| ----- | ----- | ----- | ----- | ----- | ----- | ----- |
| x | x | x | x | x | x | x |
| x | x | x | x | x | x | x |
| x | x | x | x | x | x | x |
| x | x | x | x | x | x | x |

## Identified Cleanup Areas

> _Use this section to log cleanup tasks identified during the operation_

- ...
- ...
- ...

/label ~"RTWork::PurpleOp"
/confidential
