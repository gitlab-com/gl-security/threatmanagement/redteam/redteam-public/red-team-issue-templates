> _This template is used for [Purple Team Flash Operations](https://handbook.gitlab.com/handbook/security/security-operations/red-team/purple-teaming/#flash-operations)_

## Stage Summary

This stage is when the team writes the Purple Team Flash Report. We still need to determine what parts of a typical Stealth Operation Report are valid, and which are unnecessary or excessive for this type of operation.
