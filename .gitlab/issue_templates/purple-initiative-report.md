<!-- Put the name of your initiative in the title -->

<!-- Who was involved? If collaborative between teams, list teams as well -->

# Key takeaways

<!-- Assume some very busy person won't scroll down and wants a tl;dr. What are 3 points they should know about? -->
<!-- Frame in terms of values - Results, Iteration, Efficiency etc. -->

<!-- Maybe add a nice infographic here too. People love graphics -->

# Introduction

<!-- Risk and impact. What is this work and why is it important? -->

## About tools or TTPs used

<!-- Outline what tools the exercises used with some detail (linking to even more details for those interested) -->

# Results

<!-- More detail about any results, linking to logs or other details for those interested -->
<!-- Use ATT&CK heatmap or graphs for visuals -->

# Actionable items for readers

<!-- if any -->

# Next steps

<!-- What's next? -->

# References

<!-- Links to any references or further detail -->