## Stage Summary

This stage is focused on developing the offensive capabilities required to execute each technique outlined in the adversary profile and reviewed in the tabletop exercise. Automation should be used whenever possible.

This may require the setup of a Command & Control (C2) environment, which would also be outlined in the previously-completed adversary profile.

## To-Do

- [ ] Choose an automation platform (i.e. GitLab CI or Caldera):
- [ ] Create a project to store all code/scripts:
- [ ] R&D for specific tech/subject (when required)
- [ ] Deploy C2 infrastructure (when required)
- [ ] Write and test all automation code for each technique (see next section)

-----

## Breakdown of Required TTP Development

We use GitLab child items to break down required TTP development. Each item can be treated like its own Issue, assigned a due date, map to a MR, etc.

/label ~"RTWork::StealthOp"
/confidential
