## Stage Summary

This stage tracks the work to write and deliver the final report.

The goal is to spread awareness and ensure that anyone in the company can understand the reports, even if they don't have a background in security. These ideals are [outlined in our handbook here.](https://about.gitlab.com/handbook/security/threat-management/red-team/#red-team-report-delivery).

## Report Writing: To-Do

- [ ] Complete the following sections in the [report template](report/README.md):
  - [ ] Executive Summary
  - [ ] Operation Detail
    - [ ] Attack Paths
    - [ ] Attack Narrative
    - [ ] Technique Detail (Including detection status)
    - [ ] Indicators of Compromise
  - [ ] Recommendations
- [ ] Get final proof-reading and approval from Red Team manager
- [ ] Get final proof-reading and approval from Director of Security Operations

## Report Delivery: To-Do

- [ ] Create a new project in the `Red Team internal/Stealth Operations` [group](https://gitlab.com/gitlab-com/gl-security/threatmanagement/redteam/redteam-internal/stealth-operations) and move the content of the report in this project (once ready to be shared) alongside with this issue and any other issues from the operation that are relevant to share with GitLab team members.
- [ ] As relevant, set up synchronous meetings with our Security Incident Response Team (SIRT) to go over the various attack stages in the report and review relevant alerts/detections. Two meetings should accomodate our various time zones. Additionally, if there are other people who were involved in the operation or may benefit from a synchronous meeting, consider setting one up with them as well. We'll offer to the "general public" that we're willing to meet with them to talk more about the operation when we share the report publicly.
- [ ] If relevant for the operation, create a short video summarizing the goals and outcome of the operation - this should not exceed five minutes.
- [ ] Follow the "[Red Team Report Delivery](https://handbook.gitlab.com/handbook/security/threat-management/red-team/#red-team-report-delivery)" handbook section to share an operation summary, link to the report, and the short video inside Slack (see message template below)
- [ ] If relevant, add the operation attack flow chart file to the [Navigator](https://gitlab.com/gitlab-com/gl-security/threatmanagement/redteam/redteam-internal/automation/ci-attack-navigator) project (json file into `layers` folder). A pipeline should run to update the Attack Navigator site.


### Slack Message Template

> Greetings from [the Red Team](https://handbook.gitlab.com/handbook/security/threat-management/red-team/)!
>
> We conduct security exercises that emulate real-world threats, so that GitLab can practice detecting and responding in a safe and controlled manner. These exercises are generally [done in stealth](https://handbook.gitlab.com/handbook/security/threat-management/red-team/#stealth-operations), meaning that only a small group of team members are aware of the details.
>
> We recently completed one of these operations and wanted to share the results. In this operation, we wanted to see what would happen if _overall scenario explanation_. Our goal was to understand our ability to detect and respond to this type of attack, and to identify ways to improve those capabilities.
>
> _Description of the operation in a language that is understandable by all GitLab team members and not only security members. The description should highlight what was done during the operation and how our blue team and/or any teams responded to it_
>
> We have raised a few recommendations to improve earlier detections and investigations, you can read the full report [here](_report link_).
>
> We'd appreciate your questions and feedback in our retrospective issue [here](_issue link_). Also, if you were directly or indirectly involved in the operation and would like to talk to the Red Team directly, feel free to reach out to us and we're glad to set something up!
>
> Thank you to all our trusted participants!
>
> cc: <_red-team and related-management_>

/label ~"RTWork::StealthOp"
/confidential
