> _This template is used for [Purple Team Flash Operations](https://handbook.gitlab.com/handbook/security/security-operations/red-team/purple-teaming/#flash-operations)_

## Stage Summary

This template documents the specific threat that Threat Intelligence has identified.

## Adversary Profile

### Tactics, Techniques, Procedures (TTPs)

> _This should be a detailed breakdown of how this adversary operates. When possible, try to map to MITRE ATT&CK._
>
> _While we should work to completely define the TTPs here, it is possible they may evolve throughout the operation. For example, some may be deemed unnecessary as there is no way possible to ever detect/respond to them, etc._

### Indicators of Compromise

> _List the indicators of compromise (IoCs) the Red Team expects to generate. These should be very specific data points that can be used for threat hunting and incident response. Some of these may not be generated until later stages in the attack._

- Network-based IoCs (DNS names, IP address, URLs, User Agents, Email Indicators, etc.):
  - ...
  - ...
- Host-based IoCs (File names, file hashes, file sizes, process names, etc.):
  - ...
  - ...

/label ~"RTWork::PurpleOp"
/confidential
